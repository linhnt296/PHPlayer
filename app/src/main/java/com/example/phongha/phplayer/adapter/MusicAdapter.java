package com.example.phongha.phplayer.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.phongha.phplayer.R;
import com.example.phongha.phplayer.data.Song;
import com.example.phongha.phplayer.listener.OnClick;

import java.io.IOException;
import java.util.List;

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.MyViewHolder> {
    private List<Song> moviesList;
    private OnClick onClick;

    public OnClick getOnClick() {
        return onClick;
    }

    public void setOnClick(OnClick onClick) {
        this.onClick = onClick;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, art, category;
        public View v;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.tv_title);
            art = (TextView) view.findViewById(R.id.tv_artist);
            category = (TextView) view.findViewById(R.id.tv_category);
            v = view;
        }
    }


    public MusicAdapter(List<Song> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.music_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String[] name = moviesList.get(position).getName().split("\\.");

        if (name[1].equals("mp3")) {
            holder.category.setBackgroundColor(Color.parseColor("#9C27B0"));
        } else {
            holder.category.setBackgroundColor(Color.parseColor("#388E3C"));
        }
        holder.title.setText(name[0]);
        holder.art.setText(moviesList.get(position).getTitle());
        holder.category.setText(name[1]);
        holder.v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onClick.press(position);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}