package com.example.phongha.phplayer.listener;

import java.io.IOException;

/**
 * Created by phongha on 10/4/2016.
 */
public interface OnClick {
    void press(int pos) throws IOException;
}
