package com.example.phongha.phplayer.fragment;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.phongha.phplayer.R;
import com.example.phongha.phplayer.activity.DetailActivity;
import com.example.phongha.phplayer.adapter.MusicAdapter;
import com.example.phongha.phplayer.data.Song;
import com.example.phongha.phplayer.listener.OnClick;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentMusic1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentMusic1 extends Fragment implements View.OnClickListener {
    public static MediaPlayer mMediaPlayer;
    private List<Song> mMusicList;
    private View v;
    private OnClick onClick;
    private TextView textView, tv_character, tv_name, tv_artist;
    private ImageButton btn_play, btn_prev, btn_next;
    private LinearLayout layout;
    private int currentPos, bgPos;
    private boolean isPlay, isFirstTime = true;
    private SharedPreferences sharedPreferences;
    private ProgressBar progressBar;
    private Handler mHandler = new Handler();

    public FragmentMusic1() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FragmentMusic1 newInstance() {
        FragmentMusic1 fragment = new FragmentMusic1();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_music1, container, false);

        RecyclerView mListView = (RecyclerView) v.findViewById(R.id.rv_music);
        mListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        textView = (TextView) v.findViewById(R.id.tv_null);
        tv_character = (TextView) v.findViewById(R.id.tv_character);
        tv_name = (TextView) v.findViewById(R.id.tv_name);
        tv_artist = (TextView) v.findViewById(R.id.tv_artist);
        layout = (LinearLayout) v.findViewById(R.id.layout_intent);
        layout.setBackgroundColor(intiBackground());

        btn_next = (ImageButton) v.findViewById(R.id.btnNext);
        btn_play = (ImageButton) v.findViewById(R.id.btnPlay);
        btn_prev = (ImageButton) v.findViewById(R.id.btnPrevious);

        btn_play.setOnClickListener(this);
        btn_prev.setOnClickListener(this);
        btn_next.setOnClickListener(this);
        layout.setOnClickListener(this);

        initPlayer();

        // lay danh sach bai hat
        mMusicList = getMusic();
        MusicAdapter adapter = new MusicAdapter(mMusicList);
        // onclick
        onClick = new OnClick() {
            @Override
            public void press(int pos) throws IOException {
                try {
                    Song song = mMusicList.get(pos);
                    playSong(song.getPath());
                    tv_character.setText(song.getName().substring(0, 1));
                    tv_name.setText(song.getName());
                    tv_artist.setText(song.getTitle());
                    currentPos = pos;
                    btn_play.setImageResource(R.drawable.btn_pause);
                    isPlay = true;
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
        adapter.setOnClick(onClick);
        // set adapter
        mListView.setAdapter(adapter);
        // set text view null
        if (mMusicList == null || mMusicList.size() == 0)
            textView.setVisibility(View.VISIBLE);
        else {
            Song song = mMusicList.get(sharedPreferences.getInt("LAST_POS", 0));
            tv_character.setText(song.getName().substring(0, 1));
            tv_name.setText(song.getName());
            tv_artist.setText(song.getTitle());
        }
        return v;
    }

    private void initPlayer() {
        mMediaPlayer = new MediaPlayer();
        progressBar = (ProgressBar) v.findViewById(R.id.border);
    }

    private List<Song> getMusic() {
        final Cursor mCursor = getActivity().getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Audio.Media.DISPLAY_NAME, MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.ARTIST}, null, null,
                "LOWER(" + MediaStore.Audio.Media.TITLE + ") ASC");

        List<Song> songs = new ArrayList<>();
        if (mCursor.moveToFirst()) {
            do {
                Song song = new Song();
                song.setName(mCursor.getString(0));
                song.setPath(mCursor.getString(1));
                song.setTitle(mCursor.getString(2));
                songs.add(song);
            } while (mCursor.moveToNext());
        }

        mCursor.close();

        return songs;
    }

    private void playSong(String path) throws IllegalArgumentException,
            IllegalStateException, IOException {
        mMediaPlayer.reset();
        mMediaPlayer.setDataSource(path);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.prepare();
        mMediaPlayer.start();
        // show layout player
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isPlay)
                            try {
                                if (++currentPos > mMusicList.size() - 1) currentPos = 0;
                                Song song = mMusicList.get(currentPos);
                                playSong(song.getPath());
                                tv_character.setText(song.getName().substring(0, 1));
                                tv_name.setText(song.getName());
                                tv_artist.setText(song.getTitle());
                                initProgressBar();
                                layout.setBackgroundColor(intiBackground());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                    }
                }, 1000);
            }
        });
    }

    private void initProgressBar() {
        progressBar.setMax(mMediaPlayer.getDuration());
        //Make sure you update Seekbar on UI thread
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (mMediaPlayer != null) {
                    int mCurrentPosition = mMediaPlayer.getCurrentPosition() / 1000;
                    progressBar.setProgress(mCurrentPosition);
                }
                if (isPlay)
                    mHandler.postDelayed(this, 1000);
            }
        });
    }

    private int intiBackground() {
        Random random = new Random();
        bgPos = random.nextInt(17);
        int[] colors = new int[]{getResources().getColor(R.color.red),
                getResources().getColor(R.color.pink),
                getResources().getColor(R.color.puple),
                getResources().getColor(R.color.deepPuple),
                getResources().getColor(R.color.indigo),
                getResources().getColor(R.color.blue),
                getResources().getColor(R.color.lightblue),
                getResources().getColor(R.color.cyan),
                getResources().getColor(R.color.teal),
                getResources().getColor(R.color.green),
                getResources().getColor(R.color.lightgreen),
                getResources().getColor(R.color.lime),
                getResources().getColor(R.color.yelow),
                getResources().getColor(R.color.amber),
                getResources().getColor(R.color.orange),
                getResources().getColor(R.color.deeporange),
                getResources().getColor(R.color.brow)};
        return colors[bgPos];
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPlay:
                if (isPlay) {
                    mMediaPlayer.pause();
                    btn_play.setImageResource(R.drawable.btn_play);
                    isPlay = false;
                } else {
                    if (isFirstTime) {
                        try {
                            playSong(mMusicList.get(sharedPreferences.getInt("LAST_POS", 0)).getPath());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        mMediaPlayer.start();
                    }
                    btn_play.setImageResource(R.drawable.btn_pause);
                    isPlay = true;
                    initProgressBar();
                }
                break;
            case R.id.btnPrevious:
                try {
                    if (--currentPos < 0) currentPos = mMusicList.size() - 1;
                    Song song = mMusicList.get(currentPos);
                    playSong(song.getPath());
                    tv_character.setText(song.getName().substring(0, 1));
                    tv_name.setText(song.getName());
                    tv_artist.setText(song.getTitle());
                    layout.setBackgroundColor(intiBackground());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btnNext:
                try {
                    if (++currentPos > mMusicList.size() - 1) currentPos = 0;
                    Song song = mMusicList.get(currentPos);
                    playSong(song.getPath());
                    tv_character.setText(song.getName().substring(0, 1));
                    tv_name.setText(song.getName());
                    tv_artist.setText(song.getTitle());
                    layout.setBackgroundColor(intiBackground());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.layout_intent:
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putExtra("mMusicList", (Serializable) mMusicList);
                intent.putExtra("pos", currentPos);
                startActivityForResult(intent, 1);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                // xu ly ket qua tra ve tu activity detail
            }
        }
    }
}
