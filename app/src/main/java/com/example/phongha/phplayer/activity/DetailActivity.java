package com.example.phongha.phplayer.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.phongha.phplayer.R;
import com.example.phongha.phplayer.data.Song;
import com.example.phongha.phplayer.fragment.FragmentMusic1;

import java.util.List;

public class DetailActivity extends AppCompatActivity {
    List<Song> songs;
    int pos;
    TextView tv_bg, tv_title, tv_subtitle;
    AppCompatSeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent intent = getIntent();
        songs = (List<Song>) intent.getSerializableExtra("mMusicList");
        pos = intent.getIntExtra("pos", 0);

        findView();
    }

    private void findView() {
        tv_bg = (TextView) findViewById(R.id.background);
        tv_title = (TextView)findViewById(R.id.title);
        tv_subtitle = (TextView)findViewById(R.id.subtitle);

        seekBar = (AppCompatSeekBar) findViewById(R.id.seekbar);

        tv_bg.setText(songs.get(pos).getName().substring(0, 1));
        tv_title.setText(songs.get(pos).getName());
        tv_bg.setText(songs.get(pos).getTitle());

        seekBar.setVerticalScrollbarPosition(FragmentMusic1.mMediaPlayer.getCurrentPosition());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
