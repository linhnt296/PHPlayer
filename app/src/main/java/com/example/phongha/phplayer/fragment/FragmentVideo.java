package com.example.phongha.phplayer.fragment;


import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.phongha.phplayer.R;
import com.example.phongha.phplayer.data.Song;
import com.example.phongha.phplayer.listener.OnClick;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentVideo extends Fragment {
    private List<Song> mVideoList;
    private View v;
    private OnClick onClick;
    private MediaPlayer mMediaPlayer;
    int count;
    private Cursor mCursor;


    public FragmentVideo() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v =  inflater.inflate(R.layout.fragment_video, container, false);

        return v;
    }
}
