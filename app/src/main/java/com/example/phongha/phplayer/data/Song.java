package com.example.phongha.phplayer.data;

import java.io.Serializable;

/**
 * Created by phongha on 10/6/2016.
 */
public class Song implements Serializable {
    String name, path, title;
    private Long ID;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    public Song() {
    }

    public String getTitle() {
        return title;
    }

    public Long getID() {
        return ID;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", title='" + title + '\'' +
                ", ID=" + ID +
                '}';
    }

    public Song(String name, String path, String title, Long ID) {
        this.name = name;
        this.path = path;
        this.title = title;
        this.ID = ID;
    }
}
