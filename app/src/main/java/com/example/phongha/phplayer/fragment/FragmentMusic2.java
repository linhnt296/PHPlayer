package com.example.phongha.phplayer.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.phongha.phplayer.R;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentMusic2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentMusic2 extends Fragment {
    public FragmentMusic2() {
        // Required empty public constructor
    }
    // TODO: Rename and change types and number of parameters
    public static FragmentMusic2 newInstance() {
        FragmentMusic2 fragment = new FragmentMusic2();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_music2, container, false);
    }

}
